# Project README

Welcome to the Hybris project of the Krefel project.

## Links
- Jira:
- Confluence:
- BitBucket: https://bitbucket.org/account/user/xploregroup/projects/ELKR
- ...





######## TODO finalize, update below this line  ##############


## Pre-requisites for running Gradle on G-Star corporate computers
Add `GRADLE_OPTS` as environment variable in your windows machine. You need to change the default user.home used by gradle to point your local filesystem.

As an example:

	variable name: GRADLE_OPTS
	value: -Dgradle.user.home=C:\gradle

## Hybris setup

### Installation types
There are three ways of setting up a Hybris system: setup-script, vvirtualize and classic.

#### Setup script installation
The setup script (available for Windows and Mac) will install all necessary packages for running the application. This is the fastest way to get started. Under the hood it uses vvirtualize

#### VVirtualize installation

VVirtualize downloads and installs a VM containing a running database and several other goodies that Hybris depends on.
This should work on windows/mac/linux.

#### Classic installation
Using the classic setup means installing a database yourself, initialize, load data, etc.

### Prerequisites

#### Git
You need git [https://git-scm.com/]()
A git gui client that is used a lot by people on the project: [https://www.sourcetreeapp.com/]()

#### Bitbucket access
You need read access to this repo [https://bitbucket.org/gstar-ondemand/hybris]()

#### Amazon S3 access
Get an access key and secret key for the g-star-raw-development AWS bucket. See [https://g-star.atlassian.net/browse/DEVOPS-125]()

### Scripted installation

#### S3 Access

Copy `gradle.properties.example` to `gradle.properties`.

Update gradle.properties with the access key and secret key you have received from DEVOPS.

If you have not recieved your access key and secret yet then use the key and secret from the s3 connection in local.properties.example.

#### Run setup script
Run `setup.bat` on Windows or `setup.sh` on Mac to install and start Hybris. On Windows you might need to re-run the script several times in new (administrator) cmd-windows, since the PATH-variable is not programmatically updated.

### Hybris setup using vvirtualize

This setup uses a virtual machine and works on Mac and Windows (should work on Linux, but not tested at time of writing).

The `Building frontend` steps are not included in this setup.


#### Prerequisites
You need to install two softwarepackages before you can do a quick setup:

- VirtualBox: [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
- VirtualBox Extension Pack: [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
- Vagrant: [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

User rights:


#### Download and install VM
After installing the latest version of Vagrant and virtualBox you can run the following command from the project root directory:

```
#!bash
$ ./gradlew frontend-init
$ ./gradlew vvirtualize clean all
```

This downloads a VM containing:

- Ubuntu OS
- MySQL Database
- PHPMyAdmin (mysql database manager, log in with root/root)
- Apache/PHP
- Mailcatcher (catch all mail server with webinterface)
- Memcached (key-value memory database)

The vvirtualize command will also retrieve a Hybris database dump with test data from Amazon and import it into MySQL in the VM.

#### Starting the VM
After running the vvirtualize command the VM is started automatically. You can check the VM status by starting the Oracle VM VirtualBox Manager. A VM named 'GSTAR MySQL' should be present and in running state.

You do not need to run vvirtualize often only when you want to setup a new or updated virtual machine with the database.

If you already have the virtual machine setup with a (resent) database dump, you have to start the vm yourself.
To manually start / stop the virtual machine you can use the vagrant command.

From the `[project root]/develop/VM` folder you can run the start and/or stop commands:

Start the VM

```
#!bash
$ vagrant up
```

Stop the VM

```
#!bash
$ vagrant halt
```

#### Troubleshooting the VM

*Problem: The VM does not start*

When running the vvirtualize or vagrant up command the virtual machine does not end up in running state

*Solution 1: Check if virtualization is enabled on your machine.*

- Open the Oracle VM VirtualBox Manager
- Select GSTAR MySQL image
- Press Show
- The image should start without problems. If virtualization is not enabled it will give an error popup sying that virtualization is not enabled.
- Restart your machine and enable virtualization in your BIOS.

*Solution 2: Check if the required ports are available in you machine.*

On windows it happened that port 1025 (used of mailcatcher) was already in use by a process called wininit. This is reported in the log when running the vvirtualize command.

```
#!bash
$ ./gradlew vvirtualize
...
vcreatevm: Progress: 90%
vcreatevm: ==> default: Matching MAC address for NAT networking...
vcreatevm: ==> default: Checking if box 'ubuntu/trusty64' is up to date...
vcreatevm: ==> default: Setting the name of the VM: GSTAR MySQL
vcreatevm: ==> default: Clearing any previously set forwarded ports...
vcreatevm: Vagrant cannot forward the specified ports on this VM, since they
vcreatevm: would collide with some other application that is already listening
vcreatevm: on these ports. The forwarded port to 1025 is already in use
vcreatevm: on the host machine.
vcreatevm:
vcreatevm: To fix this, modify your current project's Vagrantfile to use another
vcreatevm: port. Example, where '1234' would be replaced by a unique host port:
vcreatevm:
vcreatevm:   config.vm.network :forwarded_port, guest: 1025, host: 1234
vcreatevm:
vcreatevm: Sometimes, Vagrant will attempt to auto-correct this for you. In this
vcreatevm: case, Vagrant was unable to. This is usually because the guest machine
vcreatevm: is in a state which doesn't allow modifying port forwarding.
```

Change the ports in `[project root]/develop/VM/Vagrantfile` for example to change the mailcatcher port from  1025 to 10025:

```
config.vm.network "forwarded_port", guest: 1025, host: 10025
```

And change accordingly in you local.properties:

```
# SMTP
mail.smtp.server=localhost
mail.smtp.port=10025
```

*Solution 3: Vagrant is running multiple times*

When the process fails, it could be vagrant is still running in the background and locking for example the recreation of a new VM. To solve this open the task manager and kill all vagrant processes.

**Solution 4: Check if your Windows is a 32 or 64-bit version. **

When you're running a 32-bit Windows version, you should modify `develop\VM\Vagrantfile` and change the Ubuntu version to `trusty32`.


#### Update Hybris config

Check if you have `hybris/config/local.properties`. If not, copy the supplied `local.properties.example`:

```
#!bash
$ cp hybris/config/local.properties.example hybris/config/local.properties
```

Edit the local.properties to your prefered configuration. Samples are supplied for database/mail/memcached. At least uncomment the database part.

##### VVirtualize Database properties

	db.host=localhost
	db.name=hybris
	db.username=hybris
	db.password=hybris
	db.url=jdbc:mysql://${db.host}:33066/${db.name}?useConfigs=maxPerformance&characterEncoding=utf8
	db.driver=com.mysql.jdbc.Driver
	db.tableprefix=
	mysql.optional.tabledefs=CHARSET=utf8 COLLATE=utf8_bin
	mysql.tabletype=InnoDB

##### VVirtualize Mailcatcher properties

	mail.smtp.server=localhost
	mail.smtp.port=1025

#### VVirtualize done
You should now be able to start your Hybris instance !

The dumps may be outdated, so perform an URS after starting the server. The dumps will be updated when necessary.

#### Cleanup

When you want to work on another project and stop the VM to save memory:

```
#!bash
$ vagrant halt
```

When you want to work on another project and destroy the VM to save diskspace:

```
#!bash
$ vagrant destroy
```

## Classic Hybris setup

### Database

#### Local Mysql
Download and install MySQL.

Create a database and a user:

    -- Create a database and user without throwing an error if they already exist
    CREATE DATABASE IF NOT EXISTS gstar_db;
    GRANT ALL ON `gstar_db`.* TO 'gstar_user'@'%' IDENTIFIED BY 'gstar_password';

### Mac users
Be sure to run Hybris on Java 8. If you don't have it yet, install it. Check if you have `hybris/config/local.properties`. If not, copy the supplied `local.properties.example`:

```
#!bash
$ cp hybris/config/local.properties.example hybris/config/local.properties
```

Give Hybris extra memory by changing hybris/bin/platform/setantenv.bat (Windows) or hybris/bin/platform/setantenv.sh (OSX/Linux).
Change the value of ANT_OPTS to ```-Xmx800m -XX:MaxPermSize=512m```

Then you can start fetching all necessary Hybris stuff:

```
#!bash
$ ./gradlew
```

If the build is successful, call

```
#!bash
$ ./gradlew clean all
```

Now you're ready to run the application.

```
#!bash
$ cd hybris/bin/platform && ./hybrisserver.sh
```

Point your browser to [https://localhost:9002/hac/platform/init](https://localhost:9002/hac/platform/init) and click `Initialize` and wait.

When done, import some products:

```
#!bash
$ cd test/data/product
$ curl -X POST -H "Content-Type: text/xml" --data-binary "@products.xml" http://localhost:9001/gstarintegration/rest/product/update
```

## Hostnames for local dev
In order to make the different sales channels (outlet, b2b, employee, b2c) work and because we have a host-restriction on our Google Maps API key, we have to run our localhost also like *.raw-indigo.com.

Add this to your /etc/hosts (Mac/Linux) or C:\Windows\System32\Drivers\etc\HOSTS (Windows).

	# G-Star
	127.0.0.1 local.raw-indigo.com
	127.0.0.1 local.b2b.raw-indigo.com
	127.0.0.1 local.b2c.raw-indigo.com
	127.0.0.1 local.employee.raw-indigo.com
	127.0.0.1 local.outlet.raw-indigo.com




## Eclipse

### Decompiler

Working with Hybris requires a decompiler.
If you're using eclipse Mars version then this one works: [https://github.com/java-decompiler/jd-eclipse](https://github.com/java-decompiler/jd-eclipse)
The git clone, gradle, import worked in one go for me.

### Formatters
Use profile-gstar.xml under /develop/eclipse as formatter settings for coding. Works in both Eclipse and IntelliJ

## Automated testing using Robot framework
### System requirements
* python 2 ([latest release](https://www.python.org/downloads/release/python-2711/))

Additional for Windows

* add Python to your PATH: ```C:\Python27;C:\Python27\Scripts```

Additional for Mac OS X

* Homebrew

### Gradle one time installation

This will install the robot framework and its dependencies. If you are on Mac OSX, it will also install python using Homebrew and upgrade pip. The version shipping with Mac OS X is not the latest stable version.

```
$ ./gradlew automated-test-init
```

### Run the entire test suite
This will execute the gstarsuite using this command:

```
$ ./gradlew run-automated-test-suite
```

