#!/bin/bash

sudo yum -y install epel-release
sudo yum -y install vim lynx

sudo yum -y install python-boto git
sudo wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7
sudo rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

sudo yum -y install python-devel libffi-devel openssl-devel gcc python-pip

sudo pip install --upgrade pip
sudo pip install paramiko PyYAML Jinja2 httplib2 six
sudo pip install ansible

echo 'export ANSIBLE_HOSTS=/home/vagrant/ansible/hosts' >>~/.bash_profile

echo "Versions installed:"
python --version
pip --version
ansible --version

