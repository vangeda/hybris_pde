#!/usr/bin/env bash

apt-get install sudo
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y vim python-pip python-dev libssl-dev libffi-dev
sudo pip install -U pip setuptools cryptography ansible==2.1.1.0

echo 'export ANSIBLE_HOSTS=/home/vagrant/ansible/hosts' >>~/.bash_profile

echo "Versions installed:"
python --version
pip --version
ansible --version
