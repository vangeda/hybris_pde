#!/usr/bin/python

DOCUMENTATION = '''
---
module: add_to_overviewpage
short_description: This will add a service or application to the overviewpage
'''

EXAMPLES = '''
- name: Add Service
  add_to_overviewpage:
    type: "service"
    name: "MyService"
    port: "1234"
    hostname: "localhost"
- name: Add Application URL
  add_to_overviewpage:
    type: "application"
    name: "Name of the page"
    url: "http://myURL/"
'''

from ansible.module_utils.basic import *

def add_service_to_overviewpage(data):
    name = data['name']
    hostname = data['hostname']
    port = data['port']

    newLine = '"' + port + '";"' + name + '";"' + hostname + '"'


    with open("/var/www/html/services.csv") as file:
        for line in file:
            if newLine in line:
                return False, False

    with open("/var/www/html/services.csv", "a") as file:
        file.write(newLine + "\n")

    return False, True



def add_application_to_overviewpage(data):
    name = data['name']
    url = data['url']

    newLine = '"' + name + '";"' + url + '"'


    with open("/var/www/html/applications.csv") as file:
        for line in file:
            if newLine in line:
                return False, False

    with open("/var/www/html/applications.csv", "a") as file:
        file.write(newLine + "\n")

    return False, True




def main():

    fields = {
        "type": {
            "required": True,
            "choices": ['service', 'application'],
            "type": "str"
        },
        "hostname": {"required": False, "type": "str", "default" : ""},
        "name": {"required": True, "type": "str"},
        "port": {"required": False, "type": "str"},
        "url": {"required": False, "type": "str"}
    }

    choice_map = {
        "service": add_service_to_overviewpage,
        "application": add_application_to_overviewpage,
    }

    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed = choice_map.get(module.params['type'])(module.params)

    if not is_error:
        module.exit_json(changed=has_changed)
    else:
        module.fail_json(msg="Error adding service or application to file")


if __name__ == '__main__':
    main()