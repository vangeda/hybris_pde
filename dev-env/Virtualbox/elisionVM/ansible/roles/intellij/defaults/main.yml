---
intellij_enabled: true

# IntelliJ IDEA version number
# intellij_version can be either 'latest', or a specific version (e.g. '2016.2.4' )
intellij_version: 'latest'

# Mirror where to dowload IntelliJ IDEA redistributable package from
# Using HTTP because of https://github.com/ansible/ansible/issues/11579
intellij_mirror: "http://download.jetbrains.com/idea"

# Edition to install (community or ultimate)
intellij_edition: ultimate

# Base installation directory for any IntelliJ IDEA distribution
intellij_install_dir: /opt/idea/idea-{{ intellij_edition }}-{{ intellij_version }}

# Deprecated: split into intellij_jdk_home and users.intellij_jdks
intellij_default_jdk_home: "{{ java_home }}"

# Location of the JDK for running IntelliJ IDEA
intellij_jdk_home: "{{ java_home }}"

# Location of the default Apache Maven installation for IntelliJ IDEA projects
intellij_default_maven_home: "{{ maven_home }}"

## Default UI Configuration
# Supported themes: Darcula, GTK+, IntelliJ
intellij_ui_theme: Darcula
intellij_ui_hide_tool_stripes: false
intellij_ui_show_main_toolbar: true
intellij_ui_scroll_tab_layout_in_editor: false

# List of users to configure IntelliJ IDEA for
users:
  - username: vagrant
    intellij_jdks:
      - name: '1.{{ java_version }}'
        home: "{{ java_home }}"
    intellij_default_jdk: '1.{{ java_version }}'
    intellij_disabled_plugins:
      - org.jetbrains.plugins.gradle
      - com.intellij.uiDesigner
      - org.jetbrains.android
      - DevKit
      - Geronimo
      - GlassFish
      - JBPM
      - JBoss
      - JSR45Plugin
      - Osmorc
      - StrutsAssistant
      - WebSphere
      - Weblogic
      - com.intellij.appengine
      - com.intellij.dmserver
      - com.intellij.flex
      - com.intellij.gwt
      - com.intellij.seam
      - com.intellij.seam.pageflow
      - com.intellij.seam.pages
      - com.intellij.struts2
      - com.intellij.vaadin
      - org.intellij.grails
      - org.jetbrains.kotlin
    intellij_codestyles:
      - name: GoogleStyle
        url: 'https://raw.githubusercontent.com/google/styleguide/gh-pages/intellij-java-google-style.xml'
    intellij_active_codestyle: Elision
    intellij_plugins:
      - CheckStyle-IDEA
      - HybrisPlugin
      - ccom.intellij.idea.plugin.hybris.impex

# Directory to store files downloaded for IntelliJ IDEA installation
intellij_download_dir: "{{ x_ansible_download_dir | default(ansible_env.HOME + '/.ansible/tmp/downloads') }}"

# Timeout for IntelliJ IDEA download response in seconds
intellij_idea_download_timeout_seconds: 600
