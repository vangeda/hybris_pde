# bash_aliases

Ansible role for setting aliases in ~/.bash_aliases for Debian/Ubuntu.


## Example playbook

Lets make some aliases for ls

```yml
# playbook.yml

vars:
	# See all available variables at defaults/main.yml
	bash_aliases:
	  - { alias: 'll', command: 'ls -la' }
	  - { alias: 'llh', command: 'ls -lah' }

    # You also can add extra lines in any format to .bash_aliases
    bash_aliases_extra:
      - "git config --global alias.unstage 'reset HEAD --'"

```