# Version Control System

## Description

This installs the configured VCS.
Currently supported systems are:

- GIT
- SVN

There is no need to put username/password in the files, at the start of your provisioning, Ansible will prompt you for that input.

## Configuration

### General

Configuring this role is fairly easy:

    version_control_system_url: https://{{ vcs_user }}:{{ vcs_pass }}@bitbucket.org/{{ repository }}
    vcs_project_dir: /tmp/test
    
This will checkout the HEAD revision! If you want to checkout a specific branch:

    version_control_system_branch: development-branch
    
If you don't want to checkout your sources, put:

    version_control_system_do_checkout: false
    
Define where GIT/SVN needs to checkout:

    vcs_project_dir: /checkout/directory

### GIT

Configuration the role to use GIT:

    version_control_system: GIT

### Subversion

Configuration the role to use Subversion:

    version_control_system: SVN

# TODO fix checkout 