# Elision VirtualBox

## Description

This box is a starting point when starting a new project. 

The initial build process will take care of installing: 

- Debian 8 with graphical shell
- ansible
- firewall
- apache + php 
- ntp server
- firefox
- mailcatcher
- overview page  (http://localhost)
- VCS system (depending on the project, this could be Subversion or GIT)
- mysql (if needed)
- php-mysql (if mysql is installed)
- phpMyAdmin (if mysql is installed)
- java (version to be configured!)
- maven
- hybris (version to be configured!)
- intellij


## Requirements

- Virtualbox 5.0 should be installed on your host machine.
- Vagrant and vbguest plugin should be installed
  > vagrant plugin install vagrant-vbguest

## Usage

### Configure your virtualbox

There are 2 files which you can change in order to configure this box to your needs:

- project_properties.yml  (this contains the configuration for your VirtualBox settings)
- ansible/vars/main.yml   (this contains the configuration for the ansible roles which are included in this project. Do not change the default files in the roles itself!) 

### Building the virtualBox

When you want/need to make changes to the Box, you can adapt the Vagrantfile or add additional ansible scripts.

Build the new box, this will just create the VirtualBox, nothing will be installed yet!
```
    vagrant up
```

Setup of the applications:

    # Minimal setup (ntp, apache, php, firewall, mailcatcher, overview page)
    ansible-playbook provision-minimal.yml

    # Full setup 
    ansible-playbook provision-all.yml

Above examples will run all available tasks in the playbooks.

In order to speed things up even more, we introduced the following tags:

- pde 
- 

Create new baseBox
```
    vagrant package --base elisionBase --output box/boxname.box
```   

## Overview page

The overview page lists the state of defined running services and links to applications.
This page takes 2 CSV files (/usr/www/html/services.csv & /usr/www/html/applications.csv) 

If you want to add services/applications to this page, you can use the custom ansible module to do this.
Add the following to your ansible playbook:
```
    - name: Add hybris https service to overview page
      add_to_overviewpage:
        type: "service"
        port: "9002"
        name: "hybris (HTTPS)"
    
    - name: Add hybris hac url to overview page
      add_to_overviewpage:
        type: "application"
        url: "http://localhost:9001/"
        name: "hybris (Hybris Admin Console)"    
```


## Useful information about the box

- users:
- - root : vagrant
- - vagrant : vagrant

- links:
- - overview page: http://localhost




## TODO

## how this should work:

configure following things:

- hybris
    * [] upgrade ? (if so, "migrate existing hybris")
    * [] if not an upgrade, do checkout from source control system
    * [] use solr? --> add to overview page    --> do in separate "configure" role 
- IntelliJ
    * [] default debug configuration (to debug against hybris)

